#!/usr/bin/python3
from urllib.request import urlopen
import json
import subprocess, shlex

allProjects     = urlopen("https://gitlab.com/api/v3/projects/all?private_token=zjAToBwaMs2SEyqAHygz")
allProjectsDict = json.loads(allProjects.read().decode())
for thisProject in allProjectsDict:
    try:
        thisProjectURL  = thisProject['ssh_url_to_repo']
        print("{0}".format(thisProjectURL))
        # command     = shlex.split('git clone %s' % thisProjectURL)
        # resultCode  = subprocess.Popen(command)

    except Exception as e:
        print("Error on %s: %s" % (thisProjectURL, e.strerror))
