#!/bin/bash
# git-configure-project.sh
#
# Author: Marcel Gerber http://it.marcelgerber.ch/
#
# Clone and configure Git project with personal settings
# with SSH key set as variable.
#
# Changes
#  05.02.2018 1.00 MGE - Initial version
#

# Variables
sshkey=~/.ssh/id_rsa_gitlab_xxxxx
script=`basename "$0"`

# Check if parameter for group/project is set
if [ -z "$1" ]; then
  echo "Project name is not specified. Exiting."
  echo "Usage: $script '<group>/<project>'"
  exit 1
fi

# Separate group from project into variables
IFS='/' read -ra VAL <<< "$1"
gitgroup=${VAL[0]}
gitproject=${VAL[1]}
if [ -z "$gitproject" ]; then
  echo "Project name is empty. Exiting."
  echo "Usage: $script '<group>/<project>'"
  exit 1
fi

# Clone git repository
echo "Configuring git for project '$gitproject'."
git clone git@gitlab.com-mcd:$gitgroup/$gitproject.git

if [ ! -d "$gitproject" ]; then
  echo "There was an issue, git project '$gitproject' was not created. Exiting."
  exit 2
fi

cd $gitproject
# git config core.sshCommand "ssh -i $sshkey -F /dev/null"
git config user.name "Marcel Gerber"
git config user.email "marcel@email.com"
git pull
